"""Stream type classes for tap-smileio."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_smileio.client import SmileioStream

# TODO: Delete this is if not using json files for schema definition
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
# TODO: - Override `UsersStream` and `GroupsStream` with your own stream definition.
#       - Copy-paste as many times as needed to create multiple stream types.


class CustomersStream(SmileioStream):
    """Define custom stream."""
    name = "customers"
    path = "/customers"
    records_jsonpath = "$.customers[*]"
    primary_keys = ["id"]
    replication_key = "updated_at"
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("first_name", th.StringType),
        th.Property("last_name", th.StringType),
        th.Property("email", th.StringType),
        th.Property("date_of_birth", th.DateType),
        th.Property("points_balance",th.NumberType),
        th.Property("referral_url", th.StringType),
        th.Property("state", th.StringType),
        th.Property("vip_tier_id",th.NumberType),
        th.Property("created_at",th.DateTimeType),
        th.Property("updated_at",th.DateTimeType),
        
    ).to_dict()

class ActivitiesStream(SmileioStream):
    """Define custom stream."""
    name = "activities"
    path = "/activities"
    records_jsonpath = "$.activities[*]"
    primary_keys = ["id"]
    replication_key = "updated_at"
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("customer_id", th.NumberType),
        th.Property("activity_type", th.StringType),
        th.Property("token", th.StringType),
        th.Property("processed_at",th.DateTimeType),
        th.Property("created_at",th.DateTimeType),
        th.Property("updated_at",th.DateTimeType), 
    ).to_dict()
